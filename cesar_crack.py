import string
import random
from collections import Counter


def encrypt_cesar(key, plaintext):
    encrypted_text = ""
    for char in plaintext:
        if char.isalpha():
            shift = key % 26
            if char.islower():
                encrypted_text += chr((ord(char) - ord('a') + shift) % 26 + ord('a'))
            else:
                encrypted_text += chr((ord(char) - ord('A') + shift) % 26 + ord('A'))
        else:
            encrypted_text += char
    return encrypted_text


def decrypt_cesar(key, ciphertext):
    decrypted_text = ""
    for char in ciphertext:
        if char.isalpha():
            shift = key % 26
            if char.islower():
                decrypted_text += chr((ord(char) - ord('a') - shift) % 26 + ord('a'))
            else:
                decrypted_text += chr((ord(char) - ord('A') - shift) % 26 + ord('A'))
        else:
            decrypted_text += char
    return decrypted_text

def frequency_analysis(text):
    letters = string.ascii_letters
    text = ''.join([c for c in text if c in letters])
    freqs = Counter(text)
    return freqs.most_common()

def calculate_score(decrypted_text, english_letter_freq):
    freqs = frequency_analysis(decrypted_text)
    score = 0
    for i, (char, _) in enumerate(freqs):
        if char.upper() in english_letter_freq:
            score += abs(i - english_letter_freq.index(char.upper()))
    return score

def crack_caesar_cipher(ciphertext):
    decrypted_texts = []
    for key in range(1, 65536):
        decrypted_text = decrypt_cesar(key, ciphertext)
        decrypted_texts.append((key, decrypted_text))
    english_letter_freq = 'ETAOINSHRDLCUMWFGYPBVKJXQZ'
    min_score = float('inf')
    best_key = None

    for key, decrypted_text in decrypted_texts:
        score = calculate_score(decrypted_text, english_letter_freq)

        if score < min_score:
            min_score = score
            best_key = key

    if best_key is not None:
        return decrypt_cesar(best_key, ciphertext)
    return "Unable to crack the cipher."


def generate_key_vernam(plaintext, alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
    plaintext_length = len(plaintext)

    key = ''.join(random.choice(alphabet) for _ in range(plaintext_length))
    return key

def encrypt_vernam(plaintext, key):
    ciphertext = ''.join(chr(ord(p) ^ ord(k)) for p, k in zip(plaintext, key))
    return ciphertext


def decrypt_vernam(ciphertext, key):
    decrypted_text = ''.join(chr(ord(c) ^ ord(k)) for c, k in zip(ciphertext, key))
    return decrypted_text


if __name__ == "__main__":
    # Example usage
    KEY = 43127
    MESSAGE = "Hello, my dear! Please, tell me about you."
    VERNAM_KEY = generate_key_vernam(MESSAGE)
    encrypted_message_cesar = encrypt_cesar(KEY, MESSAGE)
    decrypted_message_cesar = decrypt_cesar(KEY, encrypted_message_cesar)
    decrypted_message2 = crack_caesar_cipher(encrypted_message_cesar)
    encrypted_message_vernam = encrypt_vernam(MESSAGE, VERNAM_KEY)
    decrypted_message_vernam = decrypt_vernam(encrypted_message_vernam, VERNAM_KEY)
    print("Original message:", MESSAGE)
    print("Encrypted message by Cesar cipher:", encrypted_message_cesar)
    print("Encrypted message by Vernam cipher:", encrypted_message_vernam)
    print("Decrypted message by Cesar:", decrypted_message_cesar)
    print("Decrypted message by Vernam:", decrypted_message_vernam)
    print("Crack message cipher by Cesar:", decrypted_message2)
